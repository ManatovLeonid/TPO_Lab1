public class SecCalculator {


    public double Calculate(double x, double percision) {


        if(Math.abs(x) >=Math.PI/2){
            return Double.NaN;
            //            throw new IllegalArgumentException("Must be |x| < PI/2 ");
        }

        double curResult = 0;
        double prevResult = 0;
        int k = 0;
        int fact = 1;

        curResult = GetMember(k++, x, fact);

        while (Math.abs(curResult - prevResult) > percision) {

            fact *= ((2 * k) * (2 * k - 1));
            if (fact <= 0) fact = 1;

            prevResult = curResult;
            curResult = curResult + GetMember(k++, x, fact);
        }

        return ((double) 1.0) / curResult;
    }


    private double GetMember(int k, double x, double factorial) {

        return (Math.pow(-1, k) * Math.pow(x, 2 * k) / factorial);
    }

    private int CalculateFactorial(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }

}
