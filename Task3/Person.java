package Task3;


import java.util.ArrayList;
import java.util.List;

public class Person {
    private String name;
    private List<Activity> activities;

    public Person(String name) {
        this.name = name;
        activities = new ArrayList<>();
    }

    public void AddActivity(Activity activity) {
        activities.add(activity);
        activity.StartActivity();
    }

    public void RemoveActivity(Activity activity) {

        if (activities.contains(activity)) {
            activities.remove(activity);
        }

    }


    public String getName() {
        return name;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Имя: ");
        stringBuilder.append(name);

        if (activities.size() > 0) {
            stringBuilder.append("\n");
            stringBuilder.append("Занимается:");
            for (Activity activity : activities) {
                stringBuilder.append("\n");
                stringBuilder.append(activity.getActivityAction().toString());
            }

        }
        return stringBuilder.toString();

    }
}



