package Task3;

import java.util.ArrayList;
import java.util.List;

public class Transport {
    private String name;
    private List<Passenger> passengers;
    private List<TransportSpot> spots;
    private PathInstance pathInstance;
    private TransportSpot initialSpot;

    public Transport(String name, TransportSpot initialSpot) {
        this.name = name;
        this.initialSpot = initialSpot;
        spots = new ArrayList<>();
        passengers = new ArrayList<>();
        spots.add(initialSpot);
    }

    public Transport(String name, TransportSpot initialSpot, List<TransportSpot> spots) {
        this.name = name;
        this.initialSpot = initialSpot;
        this.spots = spots;
        passengers = new ArrayList<>();
        if (!spots.contains(initialSpot)) {
            spots.add(initialSpot);
        }
    }

    public Transport(String name, TransportSpot initialSpot, List<TransportSpot> spots, PathInstance pathInstance) {

        this.name = name;
        this.spots = spots;
        this.pathInstance = pathInstance;
        this.initialSpot = initialSpot;
        passengers = new ArrayList<>();

        if (!spots.contains(initialSpot)) {
            spots.add(initialSpot);
        }
    }


    public String getName() {
        return name;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public List<TransportSpot> getSpots() {
        return spots;
    }

    public PathInstance getPathInstance() {
        return pathInstance;
    }

    public TransportSpot getInitialSpot() {
        return initialSpot;
    }

    public void AddPassenger(Passenger passenger) {
        if (this.passengers == null) this.passengers = new ArrayList<>();

        passenger.GetInTransport(this);
        passengers.add(passenger);
    }


    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Имя транспорта: ");
        stringBuilder.append(name);

        if (passengers.size() > 0) {

            stringBuilder.append("\n");
            stringBuilder.append("Пассажиры: ");

            for (Passenger passenger : passengers) {
                stringBuilder.append("\n");
                stringBuilder.append(passenger.getName());

            }
        }

        if (spots.size() > 0) {

            stringBuilder.append("\n");
            stringBuilder.append("Места: ");
            for (TransportSpot spot : spots) {
                stringBuilder.append("\n");
                stringBuilder.append(spot.getName());
            }

        }
        return stringBuilder.toString();

    }
}
