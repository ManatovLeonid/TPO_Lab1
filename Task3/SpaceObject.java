package Task3;

public class SpaceObject {
    private String name;
    private SpaceObjectType type;

    public String getName() {
        return name;
    }

    public SpaceObjectType getType() {
        return type;
    }

    public SpaceObject(String name, SpaceObjectType type) {
        this.name = name;
        this.type = type;
    }
}
