package Task3;

public class PathInstance {

    private PathPlan path;
    private float timePassed;
    private float remainDistance;

    public PathPlan getPath() {
        return path;
    }

    public float getTimePassed() {
        return timePassed;
    }

    public float getRemainDistance() {
        return remainDistance;
    }


    public PathInstance(PathPlan path, float timePassed, float remainDistance) {
        this.path = path;
        this.timePassed = timePassed;
        this.remainDistance = remainDistance;
    }
}
