package Task3.ActivityActions;

import Task3.Activity;
import Task3.Person;

public abstract class ActivityAction {
    public abstract void DoAction(Activity activity, Person person);
}
