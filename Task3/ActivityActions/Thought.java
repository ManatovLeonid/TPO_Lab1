package Task3.ActivityActions;

import Task3.Activity;
import Task3.Person;

public class Thought extends ActivityAction {

    private String thoughts;

    public Thought(String thoughts) {
        this.thoughts = thoughts;
    }

    public String getThoughts() {
        return thoughts;
    }

    @Override
    public void DoAction(Activity activity, Person person) {

    }


    @Override
    public String toString() {
        return "Рассуждает о " + thoughts;
    }
}
