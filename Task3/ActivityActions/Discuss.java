package Task3.ActivityActions;

import Task3.Activity;
import Task3.Person;

public class Discuss extends ActivityAction {

    private String subject;

    public Discuss(String subject) {

        this.subject = subject;
    }

    @Override
    public void DoAction(Activity activity, Person person) {

    }

    public String getSubject() {
        return subject;
    }

    @Override
    public String toString() {
        return "Обсуждает " + subject;
    }
}
