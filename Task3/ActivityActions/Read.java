package Task3.ActivityActions;

import Task3.Activity;
import Task3.Person;
import Task3.Thing;

public class Read extends ActivityAction {

    private Thing thing;

    public Read(Thing thing) {
        this.thing = thing;
    }

    @Override
    public void DoAction(Activity activity, Person person) {

    }

    public Thing getThing() {
        return thing;
    }


    @Override
    public String toString() {
        return "Читает " + thing.getName();
    }
}
