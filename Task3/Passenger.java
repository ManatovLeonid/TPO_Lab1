package Task3;

public class Passenger extends Person {

    private TransportSpot currentTransportSpot;
    private Transport currentTransport;


    public Passenger(String name) {
        super(name);
    }

    public void GetInTransport(Transport currentTransport) {

        this.currentTransportSpot = currentTransport.getInitialSpot();
        this.currentTransport = currentTransport;
    }

    public void ChangeSpot(TransportSpot newTransportSpot) {

        if(!currentTransport.getSpots().contains(newTransportSpot))
            throw new IllegalArgumentException("Transport spot not in the transport");

        this.currentTransportSpot = newTransportSpot;

    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(super.toString());
        stringBuilder.append("\n");
        if (currentTransport != null) {
            stringBuilder.append("В транспорте: ");
            stringBuilder.append(currentTransport.getName());
            stringBuilder.append("\n");
            stringBuilder.append("В месте: ");
            stringBuilder.append(currentTransportSpot.getName());
        }

        return stringBuilder.toString();
    }
}
