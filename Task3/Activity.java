package Task3;

import Task3.ActivityActions.ActivityAction;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;


public class Activity {
    private List<Person> actors;
    private boolean isActive;
    private ActivityAction activityAction;


    public Activity(List<Person> actors, ActivityAction activityAction, boolean isActive) {
        this.actors = actors;
        this.isActive = isActive;
        this.activityAction = activityAction;
    }

    public Activity(Person actor, ActivityAction activityAction, boolean isActive) {
        actors = new ArrayList<>();
        actors.add(actor);
        this.isActive = isActive;
        this.activityAction = activityAction;

    }

    public final void StartActivity() {
        isActive = true;
        DoActivityAction();
    }

    public void StopActivity() {
        isActive = true;

        for (Person actor : actors) {
            actor.RemoveActivity(this);
        }

    }

    public List<Person> getActors() {
        return actors;
    }

    public boolean isActive() {
        return isActive;
    }

    public ActivityAction getActivityAction() {
        return activityAction;
    }


    private void DoActivityAction() {
        for (Person actor : actors) {
            activityAction.DoAction(this, actor);
        }
    }
}
