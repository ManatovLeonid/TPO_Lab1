package Task3;

public class TransportSpot {
    private String name;

    public TransportSpot(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
