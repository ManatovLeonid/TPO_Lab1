package Task3;

public class PathPlan {
    private SpaceObject from;
    private SpaceObject to;

    public SpaceObject getFrom() {
        return from;
    }

    public SpaceObject getTo() {
        return to;
    }

    public PathPlan(SpaceObject from, SpaceObject to) {
        this.from = from;
        this.to = to;
    }
}
