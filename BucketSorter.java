import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public abstract class BucketSorter<T> {
    public abstract List<T> Sort(List<T> initialList);
}

