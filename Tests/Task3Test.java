
import Task3.*;
import Task3.ActivityActions.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class Task3Test {

    @Test
    public void TestPassengerInitialSpot() {

        Passenger passenger = new Passenger("passenger");

        TransportSpot initialSpot = new TransportSpot("initialSpot");
        Transport transport = new Transport("transport", initialSpot);
        transport.AddPassenger(passenger);

        Assertions.assertEquals("Имя: passenger\n" +
                "В транспорте: transport\n" +
                "В месте: initialSpot", passenger.toString());
    }

    @Test
    public void TestPassengerChangeSpot() {

        Passenger passenger = new Passenger("passenger");

        TransportSpot initialSpot = new TransportSpot("initialSpot");
        TransportSpot spot1 = new TransportSpot("spot1");
        TransportSpot spot2 = new TransportSpot("spot2");

        List<TransportSpot> spots = new ArrayList<>();
        spots.add(initialSpot);
        spots.add(spot1);
        spots.add(spot2);
        Transport transport = new Transport("transport", initialSpot,spots);

        transport.AddPassenger(passenger);


        passenger.ChangeSpot(spot1);

        Assertions.assertEquals("Имя: passenger\n" +
                "В транспорте: transport\n" +
                "В месте: spot1", passenger.toString());

        passenger.ChangeSpot(spot2);

        Assertions.assertEquals("Имя: passenger\n" +
                "В транспорте: transport\n" +
                "В месте: spot2", passenger.toString());

    }

    @Test
    public void TestPassengerTryChangeSpotOtherTransport() {

        Passenger passenger = new Passenger("passenger");

        TransportSpot initialSpot = new TransportSpot("initialSpot");
        TransportSpot spot1 = new TransportSpot("spot1");
        Transport transport = new Transport("transport", initialSpot);

        transport.AddPassenger(passenger);


        String ExceptionMessage = "";
        try {
            passenger.ChangeSpot(spot1);

        } catch (IllegalArgumentException e) {
            ExceptionMessage = e.getMessage();


        }

        Assertions.assertEquals("Transport spot not in the transport", ExceptionMessage);
    }

    @Test
    public void TestActivityOneAdd() {

        Person person = new Person("person");
        Thing thing = new Thing("thing");

        ActivityAction fixBrains = new FixBrains(thing);

        person.AddActivity(new Activity(person, fixBrains, false));


        Assertions.assertEquals("Имя: person\n" +
                "Занимается:\n" +
                "Пытается вправить себе мозги c помощью thing", person.toString());
    }


    @Test
    public void TestActivityMultiplyAdd() {

        Person person = new Person("person");
        Thing thing = new Thing("thing");

        ActivityAction activityAction1 = new FixBrains(thing);
        ActivityAction activityAction2 = new Discuss(thing.getName());
        ActivityAction activityAction3 = new Read(thing);

        person.AddActivity(new Activity(person, activityAction1, false));
        person.AddActivity(new Activity(person, activityAction2, false));
        person.AddActivity(new Activity(person, activityAction3, false));


        Assertions.assertEquals("Имя: person\n" +
                "Занимается:\n" +
                "Пытается вправить себе мозги c помощью thing\n" +
                "Обсуждает thing\n" +
                "Читает thing", person.toString());
    }

    @Test
    public void TestActivityRemove() {

        Person person = new Person("person");
        Thing thing = new Thing("thing");

        ActivityAction activityAction1 = new FixBrains(thing);

        Activity activity = new Activity(person, activityAction1, false);

        person.AddActivity(activity);
        activity.StopActivity();

        Assertions.assertEquals("Имя: person", person.toString());
    }


    @Test
    public void TestOtherActivityDidNotAdd() {

        Person person = new Person("person");
        Thing thing = new Thing("thing");

        ActivityAction activityAction1 = new FixBrains(thing);

        Activity activity = new Activity(person, activityAction1, false);


        Assertions.assertEquals("Имя: person", person.toString());
    }

    @Test
    public void TestOtherActivityRemove() {

        Person person = new Person("person");
        Thing thing = new Thing("thing");

        ActivityAction activityAction1 = new FixBrains(thing);

        Activity activity = new Activity(person, activityAction1, false);

        activity.StopActivity();

        Assertions.assertEquals("Имя: person", person.toString());
    }

    @Test
    public void TestTransportSpots() {
        List<TransportSpot> spots = new ArrayList<>();

        TransportSpot spot1 = new TransportSpot("spot1");
        TransportSpot spot2 = new TransportSpot("spot2");
        TransportSpot spot3 = new TransportSpot("spot3");
        TransportSpot spot4 = new TransportSpot("spot4");
        spots.add(spot1);
        spots.add(spot2);
        spots.add(spot3);
        spots.add(spot4);


        Transport transport = new Transport("transport", spot1, spots);

        Assertions.assertEquals("Имя транспорта: transport\n" +
                "Места: \n" +
                "spot1\n" +
                "spot2\n" +
                "spot3\n" +
                "spot4", transport.toString());

    }


    @Test
    public void TestTransportInitialSpotInList() {
        List<TransportSpot> spots = new ArrayList<>();
        TransportSpot spot1 = new TransportSpot("spot1");
        Transport transport = new Transport("transport", spot1, spots);
        Transport transport1 = new Transport("transport1", spot1);
        Transport transport2 = new Transport("transport2", spot1, spots, new PathInstance(null, 0, 0));

        Assertions.assertEquals("Имя транспорта: transport\n" +
                "Места: \n" +
                "spot1", transport.toString());

        Assertions.assertEquals("Имя транспорта: transport1\n" +
                "Места: \n" +
                "spot1", transport1.toString());

        Assertions.assertEquals("Имя транспорта: transport2\n" +
                "Места: \n" +
                "spot1", transport2.toString());

    }


    @Test
    public void TestSample() {

        List<TransportSpot> spots = new ArrayList<>();

        TransportSpot smallTree = new TransportSpot("пальмa на мостике");
        TransportSpot enter = new TransportSpot("вход");
        TransportSpot corner = new TransportSpot("уголок");
        TransportSpot room = new TransportSpot("каюта");

        spots.add(enter);
        spots.add(smallTree);
        spots.add(corner);
        spots.add(room);


        Passenger Zafod = new Passenger("Зафод");
        Passenger Ford = new Passenger("Форд");
        Passenger Trillian = new Passenger("Триллиан");
        Passenger Artur = new Passenger("Артур");


        SpaceObjectType smoky = new SpaceObjectType("Туманность");
        SpaceObjectType planet = new SpaceObjectType("Планета");

        SpaceObject HorseHead = new SpaceObject("Конская Голова", smoky);
        SpaceObject PlanetF = new SpaceObject("Планета F", planet);

        PathPlan path = new PathPlan(PlanetF, HorseHead);

        Transport GoldHeart = new Transport("Золотое Сердце", enter, spots, new PathInstance(path, 0, 1000f));

        GoldHeart.AddPassenger(Zafod);
        GoldHeart.AddPassenger(Ford);
        GoldHeart.AddPassenger(Trillian);
        GoldHeart.AddPassenger(Artur);


        ActivityAction discuss = new Discuss("жизнь и ее последствия");

        Thing bear = new Thing("пангалактический бульк-бластер");
        ActivityAction fixBrains = new FixBrains(bear);

        ActivityAction thought = new Thought("пора было начинать знакомиться с местными обычаями");

        Thing book = new Thing("Путеводитель по Галактике для автостопщиков");
        ActivityAction read = new Read(book);

        Zafod.ChangeSpot(smallTree);
        Zafod.AddActivity(new Activity(Zafod, fixBrains, false));

        Ford.ChangeSpot(smallTree);
        Trillian.ChangeSpot(smallTree);

        Activity discussing = new Activity(Arrays.asList(Ford, Trillian), discuss, true);
        Trillian.AddActivity(discussing);
        Ford.AddActivity(discussing);

        Activity thinking = new Activity(Artur, thought, true);
        Artur.AddActivity(thinking);
        Artur.ChangeSpot(room);

        Activity reading = new Activity(Artur, read, true);
        Artur.AddActivity(reading);


        Assertions.assertEquals("Имя: Зафод\n" +
                "Занимается:\n" +
                "Пытается вправить себе мозги c помощью пангалактический бульк-бластер\n" +
                "В транспорте: Золотое Сердце\n" +
                "В месте: пальмa на мостике", Zafod.toString());


        Assertions.assertEquals("Имя: Форд\n" +
                "Занимается:\n" +
                "Обсуждает жизнь и ее последствия\n" +
                "В транспорте: Золотое Сердце\n" +
                "В месте: пальмa на мостике", Ford.toString());


        Assertions.assertEquals("Имя: Триллиан\n" +
                "Занимается:\n" +
                "Обсуждает жизнь и ее последствия\n" +
                "В транспорте: Золотое Сердце\n" +
                "В месте: пальмa на мостике", Trillian.toString());


        Assertions.assertEquals("Имя: Артур\n" +
                "Занимается:\n" +
                "Рассуждает о пора было начинать знакомиться с местными обычаями\n" +
                "Читает Путеводитель по Галактике для автостопщиков\n" +
                "В транспорте: Золотое Сердце\n" +
                "В месте: каюта", Artur.toString());


    }
}