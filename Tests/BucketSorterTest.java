import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BucketSorterTest {


    @Test
    void TestSample() {

        BucketSorter sorter = new IntegerBucketSorter();


        List<Integer> unsorted = Arrays.asList(80, 50, 60, 30, 20, 10, 70, 0, 40, 500, 600, 602, 200, 15);
        List<Integer> expected = Arrays.asList(0, 10, 15, 20, 30, 40, 50, 60, 70, 80, 200, 500, 600, 602);

        List<Integer> sorted = sorter.Sort(unsorted);

        assertEquals(expected, sorted);
    }


    @Test
    void TestSortReverse() {

        BucketSorter sorter = new IntegerBucketSorter();


        List<Integer> unsorted = Arrays.asList(602, 600, 500, 200, 80, 70, 60, 50, 40, 30, 20, 15, 10, 0);
        List<Integer> expected = Arrays.asList(0, 10, 15, 20, 30, 40, 50, 60, 70, 80, 200, 500, 600, 602);

        List<Integer> sorted = sorter.Sort(unsorted);

        assertEquals(expected, sorted);
    }


    @Test
    void TestSortZeros() {

        BucketSorter sorter = new IntegerBucketSorter();


        List<Integer> unsorted = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        List<Integer> expected = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

        List<Integer> sorted = sorter.Sort(unsorted);

        assertEquals(expected, sorted);
    }


    @Test
    void TestSortZerosAndOne() {

        BucketSorter sorter = new IntegerBucketSorter();


        List<Integer> unsorted = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
        List<Integer> expected = Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

        List<Integer> sorted = sorter.Sort(unsorted);

        assertEquals(expected, sorted);
    }

    @Test
    void TestSortSorted() {

        BucketSorter sorter = new IntegerBucketSorter();


        List<Integer> unsorted = Arrays.asList(0, 10, 15, 20, 30, 40, 50, 60, 70, 80, 200, 500, 600, 602);
        List<Integer> expected = Arrays.asList(0, 10, 15, 20, 30, 40, 50, 60, 70, 80, 200, 500, 600, 602);

        List<Integer> sorted = sorter.Sort(unsorted);

        assertEquals(expected, sorted);
    }


    @Test
    void TestSortOneElement() {

        BucketSorter sorter = new IntegerBucketSorter();


        List<Integer> unsorted = Arrays.asList(0);
        List<Integer> expected = Arrays.asList(0);

        List<Integer> sorted = sorter.Sort(unsorted);

        assertEquals(expected, sorted);
    }

    @Test
    void TestSortTwoElement() {

        BucketSorter sorter = new IntegerBucketSorter();


        List<Integer> unsorted = Arrays.asList(1,0);
        List<Integer> expected = Arrays.asList(0,1);

        List<Integer> sorted = sorter.Sort(unsorted);

        assertEquals(expected, sorted);
    }


    @Test
    void TestSortEmpty() {

        BucketSorter sorter = new IntegerBucketSorter();


        List<Integer> unsorted = new ArrayList<Integer>();
        List<Integer> expected = new ArrayList<Integer>();

        List<Integer> sorted = sorter.Sort(unsorted);

        assertEquals(expected, sorted);
    }


    @Test
    void TestSortNull() {

        BucketSorter sorter = new IntegerBucketSorter();

        String exceptionMessage = "";

        List<Integer> unsorted = null;

        try {

            List<Integer> sorted = sorter.Sort(unsorted);
        } catch (IllegalArgumentException e) {
            exceptionMessage = e.getMessage();
        }

        assertEquals("initialList is null", exceptionMessage);
    }


}