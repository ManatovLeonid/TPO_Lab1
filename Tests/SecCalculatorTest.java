import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SecCalculatorTest {

    @Test
    void checkNanZone() {

        SecCalculator calculator = new SecCalculator();
        double percision = 0.001;

        for (int n = -10; n < 10; n++) {
            Assertions.assertEquals(Double.NaN, calculator.Calculate(Math.PI / 2 + Math.PI * n, percision), percision);
        }
    }
    @Test
    void checkZero() {

        SecCalculator calculator = new SecCalculator();
        double percision = 0.001;
        Assertions.assertEquals(1 / Math.cos(0), calculator.Calculate(0, percision), percision);


    }

    @Test
    void checkInside() {

        SecCalculator calculator = new SecCalculator();
        double percision = 0.0001;

        double xLeft = -Math.PI / 2;
        double xRight = Math.PI / 2;

        for (double x = xLeft + 0.01; x < xRight - 0.01; x += 0.01) {
            Assertions.assertEquals(1 / Math.cos(x), calculator.Calculate(x, percision), 0.01);
        }
    }

    @Test
    void checkCorners() {

        SecCalculator calculator = new SecCalculator();
        double percision = 0.0001;


        Assertions.assertEquals(Double.NaN, calculator.Calculate(Math.PI/2, percision), 0.01);
        Assertions.assertEquals(Double.NaN, calculator.Calculate(-Math.PI/2, percision), 0.01);


    }


    @Test
    void SampleTest() {

        SecCalculator calculator = new SecCalculator();
        double percision = 0.001;
        double x = (Math.PI / 4);

        Assertions.assertEquals(1 / Math.cos(x), calculator.Calculate(x, percision), percision);
    }


}